

const {ccclass, property} = cc._decorator;

@ccclass
export default class MainCtrl extends cc.Component {

    @property(cc.PageView)
    pageView: cc.PageView = null;

    @property(cc.Node)
    contentNode: cc.Node = null;

    onLoad () {
        this.pageView.node.width = cc.winSize.width;
        this.pageView.node.height = cc.winSize.height;

        for(let i = 0; i < 3; i++){
            let pageNode = cc.find(`page_${i+1}`, this.contentNode);
            console.log(cc.winSize);
            pageNode.width = cc.winSize.width;
            pageNode.height = cc.winSize.height;
            console.log(pageNode);
            // pageNode.x = pageNode.width / 2 + i* pageNode.width;
        }
    }

}
